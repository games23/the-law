﻿using Assets.Scripts.City.Life;
using UnityEditor;
using UnityEngine;

namespace Assets.City.Life
{
    [CustomEditor(typeof(CityNavPoint))]
    public class CityNavigationEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            CityNavPoint myScript = (CityNavPoint)target;
            if (GUILayout.Button("Add Neighbour"))
            {
                myScript.AddNeighbour();
            }

            if (GUILayout.Button("Connect Neighbour"))
            {
                myScript.Connect();
            }
        }
    }
}