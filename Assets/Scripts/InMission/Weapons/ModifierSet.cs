﻿using System;

namespace Assets.Scripts.InMission.Weapons
{
    [Serializable]
    public struct ModifierSet
    {
        public string AttachmentId;
        public float MagazineMultiplier;
        public float AimZoom;
        public float AimSpeed;
        public float AimBonus;
        public float TunnelVisionFactor;
        public float ReloadTime;
        public float Accuracy;
        public float RecoilViolence;
        public DamageModifier Damage;
    }
}
