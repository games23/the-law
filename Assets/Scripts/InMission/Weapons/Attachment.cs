﻿using Assets.Scripts.Persistence;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class Attachment : MonoBehaviour
    {
        public string AttachmentId;
        public AttachmentType AttachmentType;

        [HideInInspector]
        public ModifierSet ModifierSet;

        public void Initialize() => 
            ModifierSet = InventoryDataPersistance.GetAttachmentStats(AttachmentId);
    }
}
