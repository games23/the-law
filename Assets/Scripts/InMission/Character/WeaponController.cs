﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Character
{
    public class WeaponController : MonoBehaviour
    {
        public Transform Hand;
        public float SwapWeaponSpeed;

        private Weapon currentWeapon;
        private Animator handAnimator;
        private bool isAiming;
        private HeadController headController;
        private TunnelVision tunnel;
        private bool isSwappingWeapon;

        public Vector3 HandDefaultPosition;
        Vector3 handTargetPosition;
        
        IList<Weapon> weapons = new List<Weapon>();
        
        public void Initialize(HeadController headController, TunnelVision tunnel, IEnumerable<Weapon> equippedWeapons)
        {
            handTargetPosition = HandDefaultPosition;

            this.tunnel = tunnel;
            this.headController = headController;
            handAnimator = Hand.GetComponentInChildren<Animator>();
            handAnimator.SetFloat("SwapSpeedFactor", SwapWeaponSpeed);

            SpawnWeapons(equippedWeapons);

            enabled = true;
        }

        void Update()
        {
            if(currentWeapon != null)
                Hand.localPosition = Vector3.Lerp(Hand.localPosition, handTargetPosition, currentWeapon.CurrentStats.AimSpeed);
        }

        private void SpawnWeapons(IEnumerable<Weapon> equippedWeapons)
        {
            foreach (var w in equippedWeapons)
            {
                w.PlaceAt(Hand);
                weapons.Add(w);
                w.Initialize(headController.ViewPoint, new ModifierSet());
            }

            SwapTo(0);
        }

        public void FireAuto()
        {
            if(currentWeapon.IsAuto)
                Fire();
        }

        public void FireSemi()
        {
            if (!currentWeapon.IsAuto)
                Fire();
        }

        void Fire()
        {
            if (!isSwappingWeapon && currentWeapon && currentWeapon.IsIdle)
            {
                if (currentWeapon.IsEmpty)
                {
                    Reload();
                }
                else
                {
                    currentWeapon.Fire();
                    var recoil = currentWeapon.CurrentStats.Recoil;
                    headController.ShakeFrame(recoil.Violence, recoil.Tendency);
                }
            }
        }

        public void SwitchAim()
        {
            if (currentWeapon.IsIdle)
            {
                isAiming = !isAiming;
                handTargetPosition = isAiming ? -currentWeapon.AimPoint : HandDefaultPosition;
                RunVisualEffects(isAiming);
                currentWeapon.SetAim(isAiming);
            }
        }

        public void SwapToNext()
        {
            if (!isSwappingWeapon && currentWeapon.IsIdle)
            {
                if (isAiming)
                    SwitchAim();
                handAnimator.SetTrigger("SwapWeapon");
                StartCoroutine(WaitAndChange());
                StartCoroutine(WaitForSwap());
            }
        }

        IEnumerator WaitAndChange()
        {
            yield return new WaitForSeconds(SwapWeaponSpeed / 2);
            SwapTo(weapons.NextIndex(currentWeapon));
        }

        IEnumerator WaitForSwap()
        {
            isSwappingWeapon = true;
            yield return new WaitForSeconds(SwapWeaponSpeed);
            isSwappingWeapon = false;
        }

        void SwapTo(int weaponIndex)
        {
            foreach (var w in weapons)
                w.gameObject.SetActive(false);

            weapons[weaponIndex].gameObject.SetActive(true);
            SetCurrentWeapon();
        }

        private void RunVisualEffects(bool isAiming)
        {
            if (isAiming)
            {
                headController.AimEffect(currentWeapon.CurrentStats.AimZoom, currentWeapon.CurrentStats.AimSpeed);
                tunnel.AimEffect(currentWeapon.CurrentStats.TunnelVisionFactor, currentWeapon.CurrentStats.AimSpeed);
            }
            else
            {
                headController.ResetView(currentWeapon.CurrentStats.AimSpeed);
                tunnel.ResetView(currentWeapon.CurrentStats.AimSpeed);
            }
        }


        void SetCurrentWeapon()
        {
            currentWeapon = Hand.GetComponentInChildren<HitscanWeapon>();
        }

        public void Reload()
        {
            if (!isSwappingWeapon)
            {
                if (isAiming)
                    SwitchAim();

                currentWeapon.Reload();
            }
        }

        public void Hide() => Hand.gameObject.SetActive(false);
    }
}
