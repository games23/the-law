﻿using UnityEngine;

namespace Assets.Scripts.InMission.Character
{
    public class CharacterLook : MonoBehaviour
    {
        public float YawFactor;
        public float PitchFactor;
        public float MaxHeadPitch;
        public Transform Head;

        //private float pitch;

        public void Initialize()
        {
        }

        public void LookTo(Vector2 lookVector)
        {
            transform.Rotate(Vector3.up * lookVector.x * YawFactor * Time.deltaTime);
            var pitch = Head.localEulerAngles.x;
            pitch = pitch > 270 ? pitch - 360 : pitch; 
            pitch += PitchFactor * lookVector.y * Time.deltaTime;
            pitch = Mathf.Clamp(pitch, -MaxHeadPitch, MaxHeadPitch);

            var head = Head.localEulerAngles;
            head.x = pitch;
            head.z = 0f;
            Head.localEulerAngles = head;
        }
    }
}