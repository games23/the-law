﻿using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class Push : MonoBehaviour
    {
        private Rigidbody myBody;

        public void Initialize() => myBody = GetComponentInParent<Rigidbody>();

        public void PushTo(Vector3 direction, float intensity) => myBody.AddForce(direction * intensity);
    }
}
