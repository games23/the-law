﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.InMission.Weapons;

namespace Assets.Scripts.InMission.Common
{
    public interface Damageable
    {
        void GetHit(Damage damage);
    }
}
