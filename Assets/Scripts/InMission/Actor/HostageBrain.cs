﻿using System;
using System.Collections;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.InMission.Actor
{
    public class HostageBrain : MonoBehaviour
    {
        public float MaxDistanceToFollow;
        public float DistanceToExit;

        private Navigator navigator;
        private Transform rescuer;
        private ActorAnimator actorAnimator;
        private NavPointProvider navPoints;
        private bool isCaptive;
        private Action onExitLevel;

        private bool RescuerIsFar => 
            Vector3.Distance(transform.position, rescuer.position) > MaxDistanceToFollow;

        private bool IsCloseToExit => 
            navPoints.GetClosestEvacPoint(transform.position).DistanceTo(transform.position) < DistanceToExit;

        public void Initialize(Navigator navigator, Interactable interactable, ActorAnimator actorAnimator, NavPointProvider navPoints, Action onExitLevel)
        {
            this.navPoints = navPoints;
            this.navigator = navigator;
            this.actorAnimator = actorAnimator;
            this.onExitLevel = onExitLevel;
            actorAnimator.BindHands();
            isCaptive = true;

            transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
            interactable.Initialize(Rescue);
        }

        public void Rescue(Transform rescuer)
        {
            if (isCaptive)
            {
                actorAnimator.UnbindHands();
                this.rescuer = rescuer;
                isCaptive = false;
            }
        }

        public void HeadToExit()
        {
            rescuer = null;
            navigator.MoveTo(navPoints.GetClosestEvacPoint(transform.position).Position, onExitLevel);
        }

        void Update()
        {
            if (!isCaptive)
            {
                if(rescuer && RescuerIsFar)
                    FollowRescuer();
                if(IsCloseToExit)
                    HeadToExit();
            }
        }

        void FollowRescuer() => navigator.MoveTo(rescuer.position);

        public void Shutdown()
        {
            enabled = false;
            StopAllCoroutines();
        }
    }
}
