﻿using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class Humanoid : MonoBehaviour
    {
        public Vector3 CenterOfMassOffset;
        public Vector3 HeadOffset;
        public Transform WeaponGrab;
        public Vector3 CenterOfMass => transform.position + CenterOfMassOffset;
        public Vector3 Head => transform.position + HeadOffset;
    }
}
