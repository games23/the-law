﻿using System;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class CriminalController : MonoBehaviour
    {
        public Health Health;
        public Ragdoll Ragdoll;
        public Animator Animator;
        public ActorAnimator ActorAnimator;
        public Navigator Navigator;
        public CriminalBrain Brain;
        public ActorWeapon Weapon;
        public ActorAiming Aiming;
        public Humanoid HumanoidSelf;
        public Transform ViewPoint;
        public Eyes Eyes;

        private Action onDeath;

        public bool IsAlive { get; private set; }

        public void Initialize(CoverSpotProvider coverProvider, Humanoid target, Vector3 spawnLocation, Action onDeath, Weapon WeaponPrefab)
        {
            IsAlive = true;
            ActorAnimator.Initialize(Animator);
            Weapon.Initialize(ActorAnimator, ViewPoint, HumanoidSelf.WeaponGrab, WeaponPrefab);
            Navigator.Initialize(ActorAnimator, spawnLocation);
            Ragdoll.Initialize();
            Health.Initialize(ActorAnimator.GetHit, OnDie);
            Aiming.Initialize(Weapon, ActorAnimator, target, HumanoidSelf, ViewPoint);
            Brain.Initialize(Navigator, coverProvider, target, ActorAnimator, Aiming);
            Eyes.Initialize(Brain.Stun);

            this.onDeath = onDeath;
        }

        void OnDie()
        {
            Ragdoll.TurnOnRagdoll();
            Animator.enabled = false;
            Navigator.Shutdown();
            Brain.Shutdown();
            IsAlive = false;

            onDeath();
        }
    }
}
