﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.UI
{
    public class HurtIndicator : MonoBehaviour
    {
        public float EffectDuration;
        public Image EffectFrame;

        public void FlashEffect(int damage)
        {
            var deltaAlpha = (float) 100/damage * EffectDuration * Time.deltaTime;
            StartCoroutine(Flash(deltaAlpha, EffectDuration));
        }

        public void ShowPermanent()
        {
            var deltaAlpha = EffectDuration * Time.deltaTime;
            StartCoroutine(FadeIn(deltaAlpha, EffectDuration));
        }

        IEnumerator Flash(float deltaAlpha, float duration)
        {
            var elapsed = 0f;
            while (elapsed < duration)
            {
                elapsed += Time.deltaTime;
                AddAlpha(EffectFrame, elapsed <= duration/2 ? deltaAlpha : -deltaAlpha);
                yield return null;
            }

            SetAlpha(EffectFrame, 0);
        }

        IEnumerator FadeIn(float deltaAlpha, float duration)
        {
            var elapsed = 0f;
            while (elapsed < duration)
            {
                elapsed += Time.deltaTime;
                AddAlpha(EffectFrame, deltaAlpha);
                yield return null;
            }

            SetAlpha(EffectFrame, 1);
        }

        private void SetAlpha(Image tunnel, float alpha)
        {
            var color = tunnel.color;
            color.a = alpha;
            tunnel.color = color;
        }

        private void AddAlpha(Image tunnel, float deltaAlpha)
        {
            var targetAlpha = tunnel.color.a + deltaAlpha;
            SetAlpha(tunnel, targetAlpha);
        }
    }
}
