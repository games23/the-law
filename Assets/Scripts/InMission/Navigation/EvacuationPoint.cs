﻿using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class EvacuationPoint : MonoBehaviour
    {
        public Vector3 Position => transform.position;

        public float DistanceTo(Vector3 target) => Vector3.Distance(transform.position, target);
    }
}
