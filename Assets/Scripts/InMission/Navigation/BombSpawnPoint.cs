﻿using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class BombSpawnPoint : MonoBehaviour
    {
        public Vector3 Position => transform.position;
    }
}
