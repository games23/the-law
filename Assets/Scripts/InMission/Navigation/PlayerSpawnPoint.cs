﻿using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class PlayerSpawnPoint : MonoBehaviour
    {
        public Vector3 Position => transform.position;
    }
}
