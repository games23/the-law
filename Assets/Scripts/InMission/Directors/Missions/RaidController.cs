﻿using System;
using Assets.Scripts.City;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Player;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.Persistence;

namespace Assets.Scripts.InMission.Directors.Missions
{
    public class RaidController : MissionController
    {
        Action onLevelComplete;
        Mission mission;
        CivilianDirector civilianDirector;

        public RaidController(
            Mission mission, 
            CivilianDirector civilianDirector, 
            CriminalDirector criminalDirector,
            HUD hud,
            SpawnPointProvider spawnPoints,
            NavPointProvider navPoints,
            CoverSpotProvider coverSpots, 
            PlayerInitializer playerCharacter,
            Action onLevelComplete)
        {
            this.mission = mission;
            this.onLevelComplete = onLevelComplete;
            this.civilianDirector = civilianDirector;

            var civCount = mission.CivilianCount;
            var crimCount = mission.CriminalCount;

            civilianDirector.Initialize(spawnPoints, civCount, navPoints, hud.UpdateCivilianDeaths);
            criminalDirector.Initialize(spawnPoints, crimCount, coverSpots, playerCharacter, hud.UpdateEnemies, OnLevelSuccess);
        }
        
        public void OnLevelFailed()
        {
            ProgressPersistence.ModifyProgress(-mission.BaseReward);
            onLevelComplete();
        }

        public void OnLevelSuccess()
        {
            var reward = mission.BaseReward + (mission.RewardPerCivilian * civilianDirector.Remaining);
            ProgressPersistence.ModifyProgress(reward);
            onLevelComplete();
        }
    }
}
