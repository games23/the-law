﻿using System;
using System.Collections;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Player;
using Assets.Scripts.InMission.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.InMission.Directors
{
    public class ShootingRangeDirector : MonoBehaviour
    {
        public PlayerInitializer Player;
        public SpawnPointProvider SpawnPoints;
        public HUD HUD;
        public Interactable ExitDoor;

        void Start() => StartCoroutine(WaitAndDo(1f, InitializeAll));

        private IEnumerator WaitAndDo(float time, Action doOnWait)
        {
            yield return new WaitForSeconds(time);
            doOnWait();
        }

        void InitializeAll()
        {
            SpawnPoints.Initialize();
            Player.Ready(SpawnPoints.PlayerSpwawnPoint.transform, () => { });
            ExitDoor.Initialize(LevelFinished);
        }

        void LevelFinished(Transform _) => StartCoroutine(WaitAndGoBackToCity());

        IEnumerator WaitAndGoBackToCity()
        {
            HUD.ShowFadeOut();
            yield return new WaitForSeconds(4f);
            SceneManager.LoadScene("Cityscape");
        }
    }
}
