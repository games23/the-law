﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Actor;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Player;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Directors
{
    public class CriminalDirector : MonoBehaviour
    {
        public CriminalController ThugPrefab;
        public CriminalController BossPrefab;
        public Weapon[] Weapons;
        public Weapon[] BossWeapons;

        private IList<CriminalController> criminals = new List<CriminalController>();
        private Action<int> onCriminalDeath = (a) => { };
        private Action onAllCriminalsAreDead = () => { };

        private Weapon PickBossWeapon => BossWeapons.PickOne();
        private Weapon PickWeapon => Weapons.PickOne();

        public void Initialize(
            SpawnPointProvider spawnPoints,
            int amount,
            CoverSpotProvider coverProvider,
            PlayerInitializer player,
            Action<int> onCriminalDeath,
            Action onAllCriminalsAreDead)
        {
            this.onCriminalDeath = onCriminalDeath;
            this.onAllCriminalsAreDead = onAllCriminalsAreDead;

            Initialize(spawnPoints, amount, coverProvider, player);
        }

        public void Initialize(
            SpawnPointProvider spawnPoints,
            int amount,
            CoverSpotProvider coverProvider,
            PlayerInitializer player)
        {
            foreach (var _ in Enumerable.Range(0, amount))
            {
                var candidate = spawnPoints.GetSpawnPoint();
                if (candidate.Exists)
                {
                    var criminal = Instantiate(ThugPrefab);
                    criminals.Add(criminal);
                    criminal.transform.SetParent(transform);
                    criminal.Initialize(coverProvider, player.Character.Humanoid, candidate.Value.Position,
                        UpdateLiveCriminals, PickWeapon);
                }
            }

            UpdateLiveCriminals();
        }

        public void InitializeWithBoss(SpawnPointProvider spawnPoints,
            int amount,
            CoverSpotProvider coverProvider,
            PlayerInitializer player,
            Action onBossDead)
        {
            SpawnBoss(spawnPoints, coverProvider, player, onBossDead);
            Initialize(spawnPoints, amount, coverProvider, player);
        }

        private void SpawnBoss(SpawnPointProvider spawnPoints, CoverSpotProvider coverProvider, PlayerInitializer player, Action onBossDead)
        {
            var candidate = spawnPoints.GetSpawnPoint();
            if (candidate.Exists)
            {
                var criminal = Instantiate(BossPrefab);
                criminals.Add(criminal);
                criminal.transform.SetParent(transform);
                criminal.Initialize(coverProvider, player.Character.Humanoid, candidate.Value.Position, onBossDead, PickBossWeapon);
            }
        }

        void UpdateLiveCriminals()
        {
            var liveCriminals = criminals.Count(c => c.IsAlive);
            onCriminalDeath(liveCriminals);
            if (liveCriminals <= 0)
                onAllCriminalsAreDead();
        }

        public IEnumerable<CriminalBrain> GetActorsInRange(Vector3 source, float range) =>
            criminals.Select(c => new {brain = c.Brain, distance = Vector3.Distance(c.transform.position, source)})
                .Where(cd => cd.distance <= range)
                .Select(cd => cd.brain)
                .ToArray();
    }
}
