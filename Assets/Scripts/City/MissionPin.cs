﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.City
{
    public class MissionPin : MonoBehaviour
    {
        public Image Icon;
        public Sprite RaidIcon;
        public Sprite BombIcon;
        public Sprite HostageIcon;
        public Sprite BossIcon;

        private Mission mission;
        private Action<Mission> onPinClick;

        public void Initialize(Mission mission, Action<Mission> onPinClick)
        {
            this.mission = mission;
            this.onPinClick = onPinClick;
            SetIcon(mission.MissionType);
        }

        private void SetIcon(MissionType missionType)
        {
            switch (missionType)
            {
                case MissionType.Bomb:
                    Icon.sprite = BombIcon;
                    break;
                case MissionType.Raid:
                    Icon.sprite = RaidIcon;
                    break;
                case MissionType.Hostages:
                    Icon.sprite = HostageIcon;
                    break;
                case MissionType.Boss:
                    Icon.sprite = BossIcon;
                    break;
            }
        }

        public void OnClick() => onPinClick(mission);
    }
}