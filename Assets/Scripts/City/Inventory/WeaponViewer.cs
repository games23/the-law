﻿using System.Collections.Generic;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.City.Inventory
{
    public class WeaponViewer : MonoBehaviour
    {
        public DisplayAttachmentFactory AttachmentFactory;
        private Dictionary<string, DisplayWeapon> weaponMap;

        public void Initialize()
        {
            weaponMap = new Dictionary<string, DisplayWeapon>();
            var weapons = GetComponentsInChildren<DisplayWeapon>();

            foreach (var w in weapons)
            {
                w.Initialize();
                weaponMap.Add(w.WeaponId, w);
            }
        }

        public DisplayWeapon GetWeapon(string weaponId) => weaponMap[weaponId];

        public void ShowWeapon(string weaponId)
        {
            HideAllWeapons();
            weaponMap[weaponId].gameObject.SetActive(true);
        }

        private void HideAllWeapons()
        {
            foreach (var w in weaponMap.Values)
                w.gameObject.SetActive(false);
        }

        public void SetAttachmentToWeapon(string weaponId, AttachmentType slotType, string attachmentId)
        {
            var weapon = weaponMap[weaponId];
            if (string.IsNullOrEmpty(attachmentId))
                weapon.RemoveAttachment(slotType);
            else
            {
                var attachment = AttachmentFactory.CreateAttachment(attachmentId);
                weapon.AddAttachment(attachment);
            }
        }

        public void SetAttachmentToWeapon(string weaponId, string attachmentId)
        {
            var weapon = weaponMap[weaponId];
            var attachment = AttachmentFactory.CreateAttachment(attachmentId);
            weapon.AddAttachment(attachment);
        }
    }
}
