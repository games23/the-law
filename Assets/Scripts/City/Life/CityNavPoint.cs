﻿using System.Collections.Generic;
using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.City.Life
{
    public class CityNavPoint : MonoBehaviour
    {
        public List<CityNavPoint> Neighbours;
        public Vector3 Position => transform.position;
        public int Connector;

        public CityNavPoint GetRandomNeighbour(CityNavPoint last) => 
            Neighbours.PickOneExcept(last);

        public CityNavPoint GetRandomNeighbour() =>
            Neighbours.PickOne();

        public void AddNeighbour()
        {
            var neighbour = Instantiate(this, transform.parent);
            neighbour.ResetWith(this);
            var ord = GetComponentInParent<CityNavPointProvider>().TotalNavPoints;
            neighbour.gameObject.name = ord.ToString();
            neighbour.transform.Translate(Vector3.forward*20);
            RegisterNeighbour(neighbour);
        }

        private void ResetWith(CityNavPoint caller)
        {
            Neighbours = new List<CityNavPoint>();
            RegisterNeighbour(caller);
        }

        public  void Connect()
        {
            var candidate = GetComponentInParent<CityNavPointProvider>().GetFromName(Connector.ToString());
            RegisterNeighbour(candidate);
            candidate.RegisterNeighbour(this);
        }

        private void RegisterNeighbour(CityNavPoint neighbour)
        {
            if(!Neighbours.Contains(neighbour))
                Neighbours.Add(neighbour);
        }

        void OnDrawGizmos()
        {
            foreach (var n in Neighbours)
                Debug.DrawLine(transform.position, n.transform.position, Color.green);
        }
    }
}