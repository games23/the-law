﻿using Assets.Scripts.GameCommon;
using Assets.Scripts.Persistence;
using UnityEngine;

namespace Assets.Scripts.City.UI
{
    public class UIController : MonoBehaviour
    {
        public MissionPanel MissionPanel;
        public CharacterPanel CharacterPanel;
        public GraphicCounter ProgressBar;

        public void Initialize(int caseRequirement)
        {
            Cursor.lockState = CursorLockMode.None;
            CharacterPanel.Initialize();

            MissionPanel.Close();
            CharacterPanel.Close();
            ProgressBar.Initialize(caseRequirement, ProgressPersistence.CurrentProgress);
        }

        public void ShowMissionPanel(Mission mission)
        {
            ProgressPersistence.AssigMission(mission);
            MissionPanel.Show(mission);
        }

        public void ShowCharacterPanel() => CharacterPanel.Show();
    }
}
