﻿using Assets.Scripts.InMission.UI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.City.UI
{
    public class MissionPanel : Panel
    {
        public Text Description;
        public DynamicLabel Difficulty;

        public void Show(Mission mission)
        {
            Description.text = mission.Description;
            Difficulty.UpdateText(mission.Difficulty);
            base.Show();
        }

        public void StartMission() => SceneManager.LoadScene("Level");
    }
}
