﻿using UnityEngine;

namespace Assets.Scripts.City.UI
{
    public class Panel : MonoBehaviour
    {
        public void Show() => gameObject.SetActive(true);

        public virtual void Close() => gameObject.SetActive(false);
    }
}
