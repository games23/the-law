﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;

namespace Assets.Scripts.Persistence.DTOs
{
    [Serializable]
    public struct LoadoutData
    {
        public string PrimaryWeaponId;
        public string SecondaryWeaponId;

        public WeaponAttachmentData[] AttachmentInfo;

        public void SetAttachments(string weaponId, IEnumerable<string> attachments)
        {
            var attachmentList = AttachmentInfo.ToList();
            attachmentList.Remove(attachmentList.First(a => a.WeaponId.MatchId(weaponId)));
            attachmentList.Add(new WeaponAttachmentData
            {
                WeaponId = weaponId,
                Attachments = attachments.ToArray()
            });

            AttachmentInfo = attachmentList.ToArray();
        }
    }
}
